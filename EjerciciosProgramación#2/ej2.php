<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 2</title>
</head>
<body>
    <?php
    // Crear dos variables con cadenas de texto
    $var1 = "Ingeniería del ";
    $var2 = "software 2023!";
    
    // Concatenar las dos cadenas utilizando el operador punto (.)
    $resultado = $var1 . $var2;
    
    // Imprimir el resultado
    echo '<p>' . $resultado . '</p>';
    ?>
</body>
</html>
