<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
    // Crear una variable con el contenido '24.5'
    $variable = 24.5;

    // Determinar el tipo de datos de la variable y mostrarlo
    echo gettype($variable)."<br>";

    // Modificar el contenido de la variable a "HOLA"
    $variable = "HOLA";

    // Determinar el tipo de datos de la variable después de la modificación y mostrarlo
    echo gettype($variable) . "<br>";

    // Cambiar el tipo de la variable a entero
    settype($variable, "integer");

    // Determinar el contenido y tipo de la variable utilizando var_dump
    var_dump($variable);
    ?>
</body>
</html>
