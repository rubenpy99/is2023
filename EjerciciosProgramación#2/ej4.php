<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 4</title>
    <style>
        /* Estilo para las filas pares */
        .fila-par {
            background-color: #e2efda; /* Color gris */
        }

        /* Estilo para las filas impares */
        .fila-impar {
            background-color: #ffffff; /* Color blanco */
        }
    </style>
</head>
<body>
    <table>
        <?php
        for ($i = 1; $i <= 10; $i++) {
            $resultado = 9 * $i;
            // Determinar si la fila es par o impar para aplicar el estilo correspondiente
            $clase_fila = ($i % 2 == 0) ? "fila-par" : "fila-impar";
            echo "<tr class='$clase_fila'><td>9 x $i</td><td>=</td><td>$resultado</td></tr>";
        }
        ?>
    </table>
</body>
</html>
