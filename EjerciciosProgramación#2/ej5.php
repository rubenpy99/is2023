<?php
// Generar valores aleatorios entre 99 y 999 para las variables $a, $b y $c
$a = rand(99, 999);
$b = rand(99, 999);
$c = rand(99, 999);

// Usar el operador ternario para comparar las expresiones $a*3 y $b+$c
$resultado = ($a * 3 > $b + $c) ? "$a*3 es mayor que $b+$c" : "$b+$c es mayor o igual que $a*3";

// Imprimir el resultado
echo $resultado;
?>
