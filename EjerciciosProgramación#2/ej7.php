<?php
// Función para generar números aleatorios pares entre 1 y 10,000
function generarNumeroParAleatorio() {
    return rand(1, 5000) * 2; // Generar números pares multiplicando por 2
}

// Generar y mostrar 900 números aleatorios pares
for ($i = 1; $i <= 900; $i++) {
    $numero = generarNumeroParAleatorio();
    echo $numero . "<br>";
}
?>
