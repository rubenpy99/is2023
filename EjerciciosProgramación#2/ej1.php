<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 1</title>
</head>
<body>
    <?php
    // Crear una variable que almacene tu nombre y apellido
    $nombreApellido = "Rubén Martí";
    
    // Crear una variable que guarde tu nacionalidad
    $nacionalidad = "Paraguaya";
    
    // Usar la función echo para imprimir el nombre y apellido en negrita y en color rojo
    echo '<p><strong><font color="red">' . $nombreApellido . '</font></strong></p>';
    
    // Usar la función print para imprimir la nacionalidad subrayada
    print '<p><u>' . $nacionalidad . '</u></p>';
    ?>
</body>
</html>
