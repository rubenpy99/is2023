<?php
// Función para generar un número aleatorio
function generarNumeroAleatorio() {
    return rand(1, 1000000); // Genera números aleatorios entre 1 y 1,000,000
}

// Ciclo infinito para generar números aleatorios hasta que se cumpla la condición
while (true) {
    $numero = generarNumeroAleatorio();
    
    if ($numero % 983 === 0) {
        echo "El número $numero es divisible por 983. ¡Condición cumplida!";
        break; // Sal del ciclo cuando se cumpla la condición
    }
}
?>
