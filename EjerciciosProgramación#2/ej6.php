<?php
// Generar valores aleatorios para las notas de los parciales
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);


// Calcular el acumulado total de notas
$acumulado = $parcial1 + $parcial2 + $final1;

// Utilizar una estructura switch para determinar la nota final
$nota_final = "";
switch (true) {
    case $acumulado >= 90 && $acumulado <= 100:
        $nota_final = "Nota 5";
        break;
    case $acumulado >= 80 && $acumulado <= 89:
        $nota_final = "Nota 4";
        break;
    case $acumulado >= 70 && $acumulado <= 79:
        $nota_final = "Nota 3";
        break;
    case $acumulado >= 60 && $acumulado <= 69:
        $nota_final = "Nota 2";
        break;
    default:
        $nota_final = "Nota 1";
        break;
}

// Imprimir la nota final con saltos de línea HTML
echo "p1: " . $parcial1 . "<br>";
echo "p2: " . $parcial2 . "<br>";
echo "f: " . $final1 . "<br>";
echo "acumulado: " . $acumulado. "<br>";
echo "La nota final es: " . $nota_final;

?>
